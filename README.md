CI for nuxt demo-app
-----------

Building 2 docker images:
1. nuxtapp-full based on node:lts-apline image (116.47 MiB)
2. nuxtapp-static using mutlistage build. After build all static files moved to nginx:stable-alpine image (9.47 MiB)

Streamline branching model not implemented yet:
https://medium.com/@jbradyaudio/streamline-a-super-efficient-branching-and-ci-strategy-ffa864aa99d4

TODO:
- [ ] ~~Remove cleanup script & use $CI_COMMIT_BEFORE_SHA~~
CI_COMMIT_BEFORE_SHA not present for merge-request pipelines
