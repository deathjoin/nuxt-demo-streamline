#!/usr/bin/env bash

# check if merge request for this commit exists & has beed merged
TEST=$( \
  curl -s --header "PRIVATE-TOKEN:${GITLAB_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/commits/${CI_COMMIT_SHORT_SHA}/merge_requests" | \
  jq -r "type == \"array\" and ([.[] | select(.state == \"merged\")] | length > 0)" \
)
if [ "${TEST}" == "true" ]
then
  SOURCE_BRANCH=$( \
    curl -s --header "PRIVATE-TOKEN:${GITLAB_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/commits/${CI_COMMIT_SHORT_SHA}/merge_requests" | \
    jq -r ".[0].source_branch" \
  )
  # find how to update version
  if [[ "${SOURCE_BRANCH}" =~ ^test ]]
  then
    PATCH_TYPE="test"
  elif [[ "${SOURCE_BRANCH}" =~ ^feature ]]
  then
    PATCH_TYPE="minor"
  elif [[ "${SOURCE_BRANCH}" =~ ^bugfix ]]
  then
    PATCH_TYPE="patch"
  elif [[ "${SOURCE_BRANCH}" =~ ^hotfix ]]
  then
    PATCH_TYPE="patch"
  fi

  if [ ! -z "${PATCH_TYPE}" ]
  then
    REPO_ID=$( \
      curl -s --header "PRIVATE-TOKEN:${GITLAB_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/registry/repositories" | \
      jq -r ".[] | select(.location == \"${IMAGE_NAME}\") | .id" \
    )
    if [ ! -z "${REPO_ID}" ]
    then
      DOES_VER_EXISTS=$( \
        curl -s --header "PRIVATE-TOKEN:${GITLAB_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/registry/repositories/$REPO_ID/tags" | \
        jq -r "[.[] | select(.name | test(\"^v[0-9]+\\\.[0-9]+\\\.[0-9]+\"))] | length > 0" \
      )
      if [ "${DOES_VER_EXISTS}" == "false" ] 
      then
        NEW_VERSION="v1.0.0"
      else
        NEW_VERSION="${PATCH_TYPE}"
      fi
      echo "${NEW_VERSION}"
    fi
  fi
fi
# curl -s --header "PRIVATE-TOKEN:${GITLAB_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/repository/commits/${CI_COMMIT_SHORT_SHA}/merge_requests" | jq