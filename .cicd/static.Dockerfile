FROM node:lts-alpine as builder
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN yarn install --frozen-lockfile
COPY . /usr/src/app/
RUN yarn generate

FROM nginx:stable-alpine
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]