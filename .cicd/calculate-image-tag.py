#!/usr/bin/env python3
from pkg_resources import parse_version
from urllib.request import Request, urlopen
from urllib.error import HTTPError
from string import Template
import json, os, re

# API Endpoints
class urls:
  # merge_requests for commit
  MR_FOR_COMMIT = Template("https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/commits/$CI_COMMIT_SHORT_SHA/merge_requests")
  # registry repositories for project
  REGISTRY_REPOS_FOR_PROJECT = Template("https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/registry/repositories")
  # tags in registry repo
  TAGS_FOR_REGISTRY_REPO = Template("https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/registry/repositories/$REPO_ID/tags")

def get_api_data(url, token):
  try:
    req = Request(url)
    req.add_header('PRIVATE-TOKEN', token)
    content = urlopen(req).read()
    return json.loads(content)
  except HTTPError:
    return False

def main():
  result_tag = ''

  # check if latest (current) merge request for this commit is merged
  commit_mrs_data = get_api_data(
    url=urls.MR_FOR_COMMIT.substitute(
      CI_PROJECT_ID=ci_project_id, 
      CI_COMMIT_SHORT_SHA=ci_commit_short_sha
    ),
    token=api_token
  )
  latest_mr_is_merged = True if (commit_mrs_data[0]["state"] == "merged") else False
  source_branch_name = commit_mrs_data[0]['source_branch']

  if not latest_mr_is_merged:
    print(f"This merge request isn't merging. State: {commit_mrs_data[0]['state']}. Nothing to do.")
    return

  # check for any versions in registry
  registry_repos_for_project = get_api_data(
    token=api_token,
    url=urls.REGISTRY_REPOS_FOR_PROJECT.substitute(
      CI_PROJECT_ID=ci_project_id
    )
  )

  # no repositories in container-registry - something wrong
  if len(registry_repos_for_project) == 0:
    print(f"No repositories found for current project.")
    return

  # find repo for current image
  repository_id = 0
  for repo in registry_repos_for_project:
    if repo['location'] == image_name:
      repository_id = repo['id']
      break
  
  if repository_id == 0:
    print(f"No repository named {image_name}. Nothing to do.")
    return
  
  # find all tags in repo
  tags_in_repository = get_api_data(
    token=api_token,
    url=urls.TAGS_FOR_REGISTRY_REPO.substitute(
      CI_PROJECT_ID=ci_project_id,
      REPO_ID=repository_id
    ) 
  )
  
  # get only version tags
  version_tags = []
  for tag in tags_in_repository:
    if re.match(r"^v[0-9]+\.[0-9]+\.[0-9]+", tag['name']):
      version_tags.append(tag)
  
  # first tag?
  if len(version_tags) == 0:
    result_tag = 'v1.0.0'
  else:
    latest_tag = max(version_tags, key=lambda x: parse_version(x))
    numbers = latest_tag.split('.')

    if source_branch_name.startswith("feature"):
      pass
    if source_branch_name.startswith("bugfix"):
      pass
    if source_branch_name.startswith("hotfix"):
      pass

  print(result_tag)

if __name__ == "__main__":
  api_token = os.getenv("API_TOKEN")
  ci_project_id = os.getenv("CI_PROJECT_ID")
  ci_commit_short_sha = os.getenv("CI_COMMIT_SHORT_SHA")
  image_name = os.getenv("IMAGE_NAME")

  if api_token == None:
    raise Exception("Set API_TOKEN env.")

  if ci_project_id == None:
    raise Exception("Set CI_PROJECT_ID env.")

  if ci_commit_short_sha == None:
    raise Exception("Set CI_COMMIT_SHORT_SHA env.")
  
  main()