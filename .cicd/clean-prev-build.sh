#!/usr/bin/env bash
if [ ! -z "${CI_MERGE_REQUEST_IID}" ]
then
  # check if merge request already has successful pipelines
  CHECK=$( \
    curl -s --header "PRIVATE-TOKEN:${GITLAB_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/pipelines" | \
    jq -r "type == \"array\" and ([.[] | select(.status == \"success\")] | length > 0)" \
  )
  if [ "${CHECK}" == "true" ]
  then
    # get commit sha from latest successful pipeline from api
    # this commit is previously build image tag
    SUCCESS_COMMIT_SHA=$( \
      curl -s --header "PRIVATE-TOKEN:${GITLAB_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/pipelines" | \
      jq -r "[.[] | select(.status == \"success\")][0] | .sha[0:8]" \
    )

    # get the project repo
    REPO_ID=$( \
      curl -s --header "PRIVATE-TOKEN:${GITLAB_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/registry/repositories" | \
      jq -r ".[] | select(.location == \"${IMAGE_NAME}\") | .id" \
    )

    if [ ! -z "${REPO_ID}" ]
    then
      curl -s --request DELETE --header "PRIVATE-TOKEN:${GITLAB_API_KEY}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/registry/repositories/$REPO_ID/tags/${SUCCESS_COMMIT_SHA}" | jq
    fi
  fi
fi